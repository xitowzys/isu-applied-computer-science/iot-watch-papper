import urequests
import json
import network
import time
import machine
from machine import Pin, SoftI2C, PWM
from lcd_api import LcdApi
from i2c_lcd import I2cLcd
from time import sleep
import ntptime


def init_lcd():
    I2C_ADDR = 0x27
    totalRows = 2
    totalColumns = 16
    i2c = SoftI2C(scl=Pin(5), sda=Pin(4), freq=10000)

    return I2cLcd(i2c, I2C_ADDR, totalRows, totalColumns)

lcd = init_lcd()


def lcd_str(message, col, row):
    lcd.move_to(col, row)
    lcd.putstr(message)

def scroll_text(msg, col, row, speed):
    if (len(msg) < lcd.num_columns):
        msg += ' ' * (lcd.num_columns - len(msg))
    
    for i in range(len(msg) + 1):
        len2 = min(lcd.num_columns, len(msg) - i)
        len1 = lcd.num_columns - len2
        msg2 = ''
        if len2 > 0:
            msg2 += msg[i:i+len2]
        if len1 > 0:
            msg2 += msg[0:len1]
        lcd.move_to(col, row)
        lcd.putstr(msg2)
        sleep(speed)

def connect_network():
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)

    while True:
        lcd.putstr("Connection...")
        sleep(1)

        sta_if.connect("honor 8", "12345678")
        sleep(8)

        if sta_if.isconnected():
            break
        else:
            lcd.clear()
            lcd.putstr("Connection failed")
            sleep(3)
            lcd.clear()
            for i in range(5, 0, -1):
                lcd.putstr("Retry in: " + str(i))
                sleep(1)
                lcd.clear()

    lcd.clear()
    lcd_str("Connection", 0, 0)
    lcd_str("successful", 0, 1)
    sleep(3)
    lcd.clear()
    return sta_if

def init_time():
    rtc = machine.RTC()
    
    response = urequests.get('http://worldtimeapi.org/api/timezone/Asia/Irkutsk')
#     print(response.text)

    parsed = response.json()
    
    rtc.datetime((
        int(parsed['datetime'][0:4]),
        int(parsed['datetime'][5:7]),
        int(parsed['datetime'][8:10]),
        2,
        int(parsed['datetime'][11:13]),
        int(parsed['datetime'][14:16]),
        int(round(float(parsed['datetime'][17:26]))), 0))
    
    return rtc

def display_time(rtc, _sleep):
    lcd_str(("{:02d}:{:02d}:{:02d}".format(rtc.datetime()[4], rtc.datetime()[5], rtc.datetime()[6])), 1, 0) # 1 - padding
#     print(rtc.datetime())
    sleep(_sleep)

def display_weather(rtc, _sleep, temperature, icon_code):
    if temp[0] == '-':
        lcd_str(temp, 11, 0)
    else:
        lcd_str(temp, 12, 0)
        
    #lcd.custom_char(1282, icon)
    lcd_str(chr(icon_code), 15, 0)
    sleep(_sleep)

def init_icons():
    lcd.custom_char(1, bytearray([0x17, 0x03, 0x09, 0x10, 0x05, 0x09, 0x00, 0x00])) # 1000 sunny
    lcd.custom_char(2, bytearray([0x00, 0x0c, 0x1E, 0x1F, 0x00, 0x00, 0x00, 0x00])) # cloudy
    lcd.custom_char(3, bytearray([0x00, 0x0c, 0x1E, 0x1F, 0x0A, 0x00, 0x0A, 0x00])) # rainy
    lcd.custom_char(4, bytearray([0x02, 0x04, 0x08, 0x1F, 0x02, 0x04, 0x08, 0x10])) # lightning
    lcd.custom_char(5, bytearray([0x04, 0x15, 0x0E, 0x1F, 0x0E, 0x15, 0x04, 0x00])) # snowy
    #lcd.custom_char(10, bytearray([0x00, 0x00, 0x1B, 0x1F, 0x1F, 0x0E, 0x04, 0x00])) # heart

    #lcd.custom_char(6, bytearray([0x1e, 0x10, 0x10, 0x1e, 0x11, 0x11, 0x1e, 0x00]))  # Буква "Б"
    #lcd.custom_char(7, bytearray([0x1f, 0x11, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00]))  # Буква "Г"
    #lcd.custom_char(8, bytearray([0xf0, 0x50, 0x50, 0x90, 0x11, 0x1f, 0x11, 0x00]))  # Буква "Д"
    #lcd.custom_char(9, bytearray([0x15, 0x15, 0x15, 0x1f, 0x15, 0x15, 0x15, 0x00]))  # Буква "Ж"
    #lcd.custom_char(10, bytearray([0xe0, 0x11, 0x10, 0x20, 0x10, 0x11, 0xe0, 0x00])) # Буква "З"
    #lcd.custom_char(11, bytearray([0x11, 0x13, 0x13, 0x15, 0x19, 0x19, 0x11, 0x00])) # Буква "И"
    #lcd.custom_char(12, bytearray([0xe0, 0x00, 0x11, 0x13, 0x15, 0x19, 0x11, 0x00])) # Буква "Й"
    #lcd.custom_char(13, bytearray([0x30, 0x70, 0x50, 0x50, 0xd0, 0x90, 0x19, 0x00])) # Буква "Л"
    #lcd.custom_char(14, bytearray([0x1f, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x00])) # Буква "П"
    #lcd.custom_char(15, bytearray([0x11, 0x11, 0x11, 0xa0, 0x40, 0x80, 0x10, 0x00])) # Буква "У"
    #lcd.custom_char(16, bytearray([0x40, 0x1f, 0x15, 0x15, 0x1f, 0x40, 0x40, 0x00])) # Буква "Ф"
    #lcd.custom_char(17, bytearray([0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x1f, 0x10])) # Буква "Ц"
    #lcd.custom_char(18, bytearray([0x11, 0x11, 0x11, 0xf0, 0x10, 0x10, 0x10, 0x00])) # Буква "Ч"
    #lcd.custom_char(19, bytearray([0x15, 0x15, 0x15, 0x15, 0x15, 0x15, 0x1f, 0x00])) # Буква "Ш"
    #lcd.custom_char(20, bytearray([0x15, 0x15, 0x15, 0x15, 0x15, 0x15, 0x1f, 0x10])) # Буква "Щ"
    #lcd.custom_char(21, bytearray([0x10, 0x10, 0x10, 0x1e, 0x11, 0x11, 0x1e, 0x00])) # Буква "Ь"
    #lcd.custom_char(22, bytearray([0x11, 0x11, 0x11, 0x19, 0x15, 0x15, 0x19, 0x00])) # Буква "Ы"
    #lcd.custom_char(23, bytearray([0x12, 0x15, 0x15, 0x1d, 0x15, 0x15, 0x12, 0x00])) # Буква "Ю"
    #lcd.custom_char(24, bytearray([0xf0, 0x11, 0x11, 0xf0, 0x50, 0x90, 0x11, 0x00])) # Буква "Я"

    #lcd.custom_char(1282, icon)

def display_product_scroll(msg, i):
    len2 = min(lcd.num_columns, len(msg) - i)
    len1 = lcd.num_columns - len2
    msg2 = ''
    if len2 > 0:
        msg2 += msg[i:i+len2]
    if len1 > 0:
        msg2 += msg[0:len1]
    lcd.move_to(0, 1)
    lcd.putstr(msg2)
        
    
test_product = []

def get_weather():
    weather_url = "http://api.weatherapi.com/v1/current.json?key=929c15979c9d462f8fa41454222205&q=Irkutsk&aqi=no"
    response = urequests.get(weather_url)
    # remove
    print(response.text)
    parsed = response.json()
    temp = str(int(parsed["current"]["temp_c"]))
    weather_status_code = parsed["current"]["condition"]["code"]
    return (temp, weather_status_code)

def get_weather_icon(weather_status):
    icon = bytearray([0x00,0x00,0x1B,0x1F,0x1F,0x0E,0x04,0x00])
    return icon

def get_products():
    
    url = "http://89.22.227.87:3000/api/check-products"
    login = "Vesnin"
    psw = "7f7ameJ5{}vZ/MqJ"
    
    headers = {"login": login, "p": psw} # test:test
    headers = {'Authorization': 'Basic VmVzbmluOjdmN2FtZUo1e312Wi9NcUo='} # test:test
    r = urequests.get(url, headers=headers)
    parsed = r.json()
    promotions_len = len(parsed)
    #print("len: " + str(promotions_len))
    #print(type(promotions_len))
    products_list = [parsed[str(i+1)]for i in range(promotions_len)]
    print(products_list)
    if promotions_len == 0:
        return ["no promotions"], 0
    else:
        return products_list, promotions_len


status_code_to_icon = {1000: 1, 1003: 2, 1006: 2, 1009: 2, 1030: 2, 1063: 3, 1066: 5, 1069: 5,
                           1072: 3, 1087: 4, 1114: 5, 1117: 5, 1135: 2, 1147: 2, 1150: 3, 1153: 3,
                           1168: 3, 1171: 3, 1183: 3, 1186: 3, 1189: 3, 1192: 3, 1195: 3, 1198: 3,
                           1201: 3, 1204: 3, 1207: 3, 1210: 5, 1213: 5, 1216: 5, 1219: 5, 1222: 5,
                           1237: 5, 1237: 5, 1240: 3, 1246: 5, 1249: 5, 1252: 5, 1255: 5, 1258: 5,
                           1261: 5, 1264: 5, 1273: 4, 1276: 4, 1279: 4, 1282: 4}

if __name__ == "__main__":
    init_icons()
    sta_if = connect_network()
    sleep(2)
    
    
    temp, weather_status_code = get_weather()
    weather_icon_code = status_code_to_icon[weather_status_code]
    
    rtc = init_time()
    
    

    start_time = time.ticks_ms()
    interval = 0
    between_message_interval = 2000
    cur_message_index = 0
    
    weather_update_interval = 900_000 # 15 min
    weather_timer = time.ticks_ms()
    
    time_update_interval = 900_000 # 15 min
    time_timer = weather_timer
    
    #promotions_update_interval = 900_000
    promotions_update_interval = 600_00
    promotions_timer = weather_timer
    # starting values
    cur_promotions_len = 0

    msg = "Initialization"
    show_message = True
    empty_string = ' ' * 16
    messages, messages_len = get_products()
    
    if messages_len != 0:
        # buzzer
        beeper = PWM(Pin(14), freq=440, duty=512)
        time.sleep(0.25)
        beeper.deinit()
    cur_promotions_len = messages_len
    while True:
        # updating time
        if time.ticks_ms() - time_timer >= time_update_interval:
            rtc = init_time()
            time_timer = time.ticks_ms()
            print("updated time")
            
        # updating weather
        if time.ticks_ms() - weather_timer >= weather_update_interval:
            temp, weather_status_code = get_weather()
            weather_icon_code = status_code_to_icon[weather_status_code]
            weather_timer = time.ticks_ms()
            print("updated weather")
            
        # updating weather
        if time.ticks_ms() - promotions_timer >= promotions_update_interval:
            messages, messages_len = get_products()
            promotions_timer = time.ticks_ms()
            print("updated promotions")
            if cur_promotions_len < messages_len:
                # buzzer
                beeper = PWM(Pin(14), freq=440, duty=512)
                time.sleep(0.20)
                beeper.deinit()
                messages_len = cur_promotions_len
        # showing everything
        if True:
            start_time = time.ticks_ms()
            i = -1
            #messages = ["promotion_0", "VEEEEEEEEERY_LOOOOOOOOOOONG_PROMOTION_111111"]
            #messages = []
            
            interval_text_show = 7000
            start_time_text_show = time.ticks_ms()
            
            interval_between_messages = 1000
            
            if show_message:
                if not sta_if.isconnected():
                    lcd_clear()
                    sta_if = connect_network()
                while True:
                    
                    if (len(msg) < lcd.num_columns):
                        display_time(rtc, 0.05)
                        lcd_str(msg, 0, 1)
                        display_weather(rtc, 0.05, temp, weather_icon_code)
                        if time.ticks_ms() - start_time_text_show >= interval_text_show:
                            #lcd.clear()
                            lcd_str(empty_string, 0, 1)
                            start_time = time.ticks_ms()
                            interval = 0
                            show_message = False
                            message_pause_start = time.ticks_ms()
                            break
                    else:
                        display_time(rtc, 0.1)
                        display_weather(rtc, 0.05, temp, weather_icon_code)
                        interval = 0

                        display_product_scroll(msg, i)
                        i = i + 1
                        # print(i, len(msg) + 1)
                        if i == len(msg):
                            sleep(1)
                            start_time = time.ticks_ms()
                            interval = 0
                            #lcd.clear()
                            lcd_str(empty_string, 0, 1)
                            show_message = False
                            message_pause_start = time.ticks_ms()
                            break
                    
                        
            else:
                # print("message changed")
                if time.ticks_ms() - message_pause_start >= interval_between_messages:
                    show_message = True
                    cur_message_index += 1
                    if cur_message_index >= len(messages):
                        cur_message_index = 0
                    if len(messages) == 0:
                        msg = "no promotions"
                    else:
                        msg = str(cur_message_index) + " " + messages[cur_message_index]
                    interval = 0                    
        else:
            display_time(rtc, 0.78)

